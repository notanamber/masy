#!/bin/bash
# Copyright 2021 Luca Paris
#This file is part of masync.

#masync is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#masync is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with masync.  If not, see <http://www.gnu.org/licenses/>.
source ~/bin/.filetemplates.sh
source ~/bin/.colordef.sh

# $1 -> hash sync
# $2 -> LOCAL PATH 
# $3 -> REMOTE PATH -> user@myvps:/home/user/syncdir/ [trailing / avoids to copy remote folder in source]
sync() {
  src=$2
  dest=$3
  otheropts=$4
  logfile=$(format ${LOGFILERSYNC} hash=$1)
  syncloopfile=$(format ${SYNCLOOPFILE} hash=$1)
  if [ ! -f $logfile ]; then 
    touch $logfile
  fi
  # PUSH TO REMOTE - and PULL FROM REMOTE swapping `src` with `dest`
  #echo "executing rsync -aPu --log-file=$LOGFILE -e ssh $otheropts $src $dest"
  # NOTE REMOTEDIR ALREADY EXISTS IN DESTINATION
  # in this form rsync receive only data from source 1>$syncloopfile 2>&1
  rsync -i -aPu --progress --out-format="%i ${GREEN}%n%L${ENDCOLOR} %''b" --log-file=$logfile -e ssh $otheropts $src $dest | \
    grep -E "(<|>|deleting)" | \
    sed -E  "s~<(\w|\W){10}~\\${PURPLE}\[SEND\]\\${ENDCOLOR} to remote ~g" | \
    sed -E  "s~>(\w|\W){10}~\\${PURPLE}\[RECEIVE\]\\${ENDCOLOR} from remote ~g" | \
    sed -E  "s~\*deleting(\s)*~\\${PURPLE}\[DELETE\]\\${ENDCOLOR} in local ~g" | \
    xargs -r -0 printf 1>>$syncloopfile 2>&1
  # last command execution exit code $?"
  rsync_result=$?
  if [[ ${rsync_result} -ne 0 ]]; then
    echo "Generic sync error. rsync has failed with the code ${rsync_result}"
    exit 1
  fi
}
