# masync

Masync allows you to mirror and keep synchronised an arbitraries number of folders on one or more remote servers with local folders.

## dependencies

Masync use inotifywait for listening changes on local folders and keep it in sync with remote folders.
Before using it, please install **inotify-tools** package on your favorite distribution

   ```
   apt-get install inotify-tools
   dnf install inotify-tools
   xbps-install -S inotify-tools
   ...
   ```

You must have access to the servers containing the remote folders via ssh and a private key, so

* install the ssh daemon on your server
* generate a key pair with ssh-keygen
* add the public key to the authorised keys on server side
* edit the ssh config so that you can access with ssh to the server

## How to use

Copy files *masync*, *syncloop.sh* and *.synccmd.sh* in `~/bin/`

make sure in the .bashrc there is something like this

`export PATH=~/bin/:$PATH`

## Using the command

###### init
Typical use of tool is starting a sync with the command

`masync init -l /home/luca/localsync/ -r remoteuser@server:/home/remoteuser/sync/`

-l option stands for the local folder you want to mirror and sync, -r option stands for the remote folder in the form known to both ssh and scp

###### list

it shows the list of all synched local folders with respective remote folder and some other usefull information: the integer id assigned to the sync,
the current status of sync, the local folder you are keeping in sync and the remote folder.
In the following commands you can refer the sync to apply the command with the integer id of sync using the option -s,
or with the local folder using the option -l

###### stop

If you want to stop a sync in the list of syncs, you can refer to it either by its id or by the local folder associated with the sync

`masync stop -s 1 or masync stop -l /home/luca/localsync/`

###### start

If you want to resume a stopped sync in the list of syncs, you can refer to it either by its id or by the local folder associated with the sync

`masync start -s 1 or masync start -l /home/luca/localsync/`

###### remove

Remove the sync from the list of all syncs, do not delete any local folder and data
		
`masync remove -s 1 or masync remove -l /home/luca/localsync/`

