class PrettyTable():
  def __init__(self):
    self.col_sep = '\uFF5C'
    self.hor_fill = '\u2015'
    self.pad_right = 5
    self.head_str = ''
    self.body_str = ''
    self.sep_str = ''
    self.hspecs = []

  def get_separator_str(self):
    if self.sep_str:
      return self.sep_str

    self.sep_str = self.col_sep
    for _, thead_len in self.hspecs:
      self.sep_str += '{0:{fill}<{flen}}'.format('',fill=self.hor_fill, flen=thead_len)
      self.sep_str += self.col_sep
    self.sep_str += '\n'
    return self.sep_str

  def head(self, hspecs):
    '''
    @param      hspecs      [('header_text', int)]      text and length of column
    '''

    self.hspecs = hspecs
    self.head_str = self.get_separator_str()
    # for thead_text, thead_len, align in zip(htexts, hlens, ['^']*len(hlens)):
    put_sep = True
    for (thead_text, thead_len), align in zip(hspecs, ['^']*len(hspecs)):
      if put_sep: 
        self.head_str += self.col_sep
        put_sep = False
      self.head_str += "{0:{align}{flen}}".format(thead_text, align=align, flen=thead_len)
      self.head_str += self.col_sep
    self.head_str += '\n'
    self.head_str += self.get_separator_str()
    # print(self.head_str)
    
  def body(self, rows):
    hlens = [thead_len for (thead_text, thead_len) in self.hspecs]

    self.body_str = ''
    for row in rows:
      row_str = self.col_sep
      for trow_text, flen in zip(row, hlens):
        row_str += "{0:{flen}}".format(' %s' % trow_text, flen=flen)
        row_str += self.col_sep
      self.body_str += row_str + '\n' + self.get_separator_str()
    # print(self.body_str)

  def print(self):
    return print(self.head_str + self.body_str)
