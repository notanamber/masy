import os

def parse_conf_line(conf_line):
  conf_line = conf_line.rstrip('\n')
  conf_line_split = conf_line.split(' ')
  k, val = conf_line_split[0], conf_line_split[-1]
  return k, val

def read_conf_section(filepath: str, section: str):
  conf_lines = []
  section_line = '[%s]' % (section,)

  with open(filepath, 'r') as fconf:
    conf_lines = fconf.readlines()

  conf_lines_section = []
  append_to_section_conf = False

  for conf_line in conf_lines:
    if conf_line == '\n' or conf_line == ' ': 
      continue

    if conf_line.startswith('[') and append_to_section_conf:
      ## section is changed
      break

    if section_line in conf_line:
      append_to_section_conf = True
      continue

    if not append_to_section_conf:
      continue

    conf_lines_section.append(conf_line)

  conf = {}

  for cline in conf_lines_section:
    k,v = parse_conf_line(cline)
    conf[k] = v

  return conf

def read_conf(filepath: str, section: str=None):
  '''
  @filepath          str                  path of config file
  @section           str                  section name in a config file [snapshot]
  '''
  if not os.path.isfile(filepath):
    print('Path %s is not a file or not exists' % (filepath,))
    return {}

  if section:
    return read_conf_section(filepath, section)
  
  conf = {}

  with open(filepath, 'r') as fconf:
    reading = True
    while reading:
      conf_line = fconf.readline()

      if not conf_line:
        break

      if conf_line == '\n' or ('[' in conf_line):
        continue

      k, val = parse_conf_line(conf_line)
      conf[k] = val

  return conf
