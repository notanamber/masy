from lib.snapshot import diff



tree_a = {
  "taglioCapelli4.jpg": "5af62402ec7716e8d729b0683061f0c4",
  "taglioCapelli5.jpg": "9c42961af589a279c4c828925291153b",
  "pino.txt": "d41d8cd98f00b204e9800998ecf8427e",
  "taglioCapelli6.jpg": "4059905eab817c33ee48f912af80fdb7",
  "spartiti_sepu": {
    "amber.py": "a6ef147511264ecdc7d6061979892c44",
    "bo.txt": "d7aa6cbc6aa0ace3225ee1738b2d5c6b",
    "ciao": {
      "amber.py": "a6ef147511264ecdc7d6061979892c44",
      "bo.txt": "d7aa6cbc6aa0ace3225ee1738b2d5c6b"
    },
    "added_folder": {}
  },
  "preferiti.txt": "af45981ef2be534dbb37f96833d3fd04"
}

tree_b = {
  "taglioCapelli4.jpg": "5af62402ec7716e8d729b0683061f0c4",
  "taglioCapelli5.jpg": "9c42961af589a279c4c828925291153b",
  # "pino.txt": "d41d8cd98f00b204e9800998ecf8427e",
  "pino" : {
    "a" : {
    },
    "c" : {"5af62402ec7716e8d729b068fasdf"},
    "d" : {"1234f02ec7716e8d729b068fasdf"},
  },
  "taglioCapelli6.jpg": "4059905eab817c33ee48f912af80fdb7",
  "spartiti_sepu": {
    "amber.py": "a6ef147511264ecdc7d6061979892c44",
    "bo.txt": "d7aa6cbc6aa0ace3225ee1738b2d5c6b",
    "added_folder": {
      "preferiti.txt": "af45981ef2be534dbb37f96833d3fd03"
    }
  },
  "preferiti.txt": "af45981ef2be534dbb37f9fffffffff"
}


def test_raw_diff():
  return diff.raw_diff(tree_a, tree_b)
