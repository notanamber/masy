import unittest

from lib.repr.node import TreeNode, RemoteTreeNode
from lib.repr.node import node_factory


class UnitTestTreeNode(unittest.TestCase):

  def setUp(self):
    name = "added_local_file.txt"
    rel_path = "./sharednotes_dev/spartiti_sepu/added_local/"
    base_path = "/home/luca/"
    cur_type = None
    cur_hash = None
    self.tree_node = TreeNode(name, rel_path, base_path, cur_type, cur_hash)

  def test_absolute_path(self):
    treeNode = self.tree_node
    abs_path = "/home/luca/sharednotes_dev/spartiti_sepu/added_local/added_local_file.txt"
    self.assertEqual(treeNode.absolute_path(), abs_path)

  def test_relative_path(self):
    treeNode = self.tree_node
    rel_path = "./sharednotes_dev/spartiti_sepu/added_local/added_local_file.txt"
    self.assertEqual(treeNode.relative_path(), rel_path)

  def test_node_factory(self):
    name = "added_local_file.txt"
    rel_path = "./sharednotes_dev/spartiti_sepu/added_local/"
    base_path = "/home/luca/"
    d = {
        'rel_path': rel_path,
        'name': name,
        'base_path': base_path,
    }

    self.treeNode = node_factory(d, base_path)
    self.assertEqual(self.treeNode.absolute_path(),
                     TreeNode(name, rel_path, base_path).absolute_path())

    self.assertEqual(self.treeNode.relative_path(),
                     TreeNode(name, rel_path, base_path).relative_path())

class UnitTestRemoteTreeNode(unittest.TestCase):
  def setUp(self):
    name = "added_local_file.txt"
    rel_path = "./spartiti_sepu/added_local/"
    base_path = "/home/notanamber/notes_dev/"
    hostname = "myvps"
    username = "notanamber"
    self.tree_node = RemoteTreeNode(name, rel_path, base_path, hostname=hostname, username=username)

  def test_isfile(self):
    self.assertEqual(self.tree_node.is_file(), True)
