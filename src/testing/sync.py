from iface import sync_manager

def get_test_manager():
  m = sync_manager.Manager(local_path='/home/luca/sharednotes_dev', remote_path='notanamber@myvps:/home/notanamber/notes_dev')
  return m

def test_init_sync():
  m = sync_manager.Manager(local_path='/home/luca/sharednotes_dev', remote_path='notanamber@myvps:/home/notanamber/notes_dev')
  m.init_sync()

def test_sync():
  '''
    notanamber@e65109ddf4:~/notes_dev$ rm -rf spartiti_sepu/added_local/
    notanamber@e65109ddf4:~/notes_dev$ rm -rf spartiti_sepu/added_remote/added_file.txt
  '''
  m = get_test_manager()
  m.sync()
