from testing import sync
from iface import snap as _snap

from lib.snapshot import diff as _diff
from lib.tree import treehash
from lib.tree import diff as _treediff

from tools import pretty_table

def table_formatter(heads , tree_diff):

  rows = []
  mlen = []

  for node_path, node_dict  in tree_diff.items():
    row = [node_path]
    mlen.append(len(node_path))

    if node_dict.get('tree_a') is not None:
      row.append('x')
    else:
      row.append('-')

    if node_dict.get('tree_b') is not None:
      row.append('x')
    else:
      row.append('-')
    
    rows.append(row)

  padding_right = 5
  hflen = max(mlen) + padding_right
  hsclen = 11 + padding_right
  htclen = 11 + padding_right

  t = pretty_table.PrettyTable()
  # t.head([('Node', hflen), ('Local', hsclen), ('Remote', htclen)])
  t.head(list(zip(heads, [hflen, hsclen, htclen])))
  t.body(rows)
  t.print()


def human_tree_diff_local_remote():
  m = sync.get_test_manager()
  local_tree_hash = m.compute_local_hash()

  remote_path = m.remote_path

  agent = m.get_agent()
  remote_tree_hash = agent.generate_tree_hash_oversftp(remote_path)


  tree_diff = _diff.raw_diff(local_tree_hash, remote_tree_hash)

  table_formatter(['Node', 'Local', 'Remote'], tree_diff)
  return tree_diff

def human_tree_diff_local():
  m = sync.get_test_manager()
  local_hash_before = m.load_local_hash()
  local_hash_after = m.compute_local_hash()

  tree_diff = _diff.raw_diff(local_hash_before, local_hash_after)
  table_formatter(['Node', 'Local Before', 'Local After'], tree_diff)
  return tree_diff


def human_tree_structure_diff_local_remote():

  m = sync.get_test_manager()
  a = m.get_agent()

  local_structure = treehash.generate_tree_structure('/home/luca/sharednotes_dev/')

  tree_diff = _treediff.raw_structure_diff(local_structure, a.generate_tree_structure_oversftp('/home/notanamber/notes_dev/'))
  table_formatter(['Node', 'Local node', 'Remote node'], tree_diff)
