import os

from lib.snapshot.generate import local as _genlocal

from tools import pretty_print
from testing import sync

def test_tree_hash():
  m = sync.get_test_manager()
  agent = m.get_agent()
  ## verify 
  local_node_hash = _genlocal.generate_tree_hash('/home/luca/sharednotes_dev/')

  remote_node_hash = agent.generate_tree_hash_oversftp('/home/notanamber/notes_dev/')

  try:
    assert local_node_hash == remote_node_hash
    print('Test executed succesfully')
    return
  except:
    pretty_print.pp(local_node_hash)
    print('*************************')
    print('*************************')
    print('*************************')
    print('*************************')
    pretty_print.pp(remote_node_hash)

def test_add_folder():
  new_path = '/home/luca/sharednotes_dev/test_add/test_add_nested'
  try:
    os.makedirs(new_path)
  except FileExistsError:
    print(f'{new_path} already exists skip creation')

  m = sync.get_test_manager()
  m.sync()

  return test_tree_hash()

def test_remove_folder():
  '''
    remove /home/luca/sharednotes_dev/test_add/
  '''
  m = sync.get_test_manager()
  m.sync()
  ## verify 
  test_tree_hash()

def test_add_file():
  '''
    add manually a file then 
    launch test_add_file
  '''
  m = sync.get_test_manager()
  m.sync()

  ## verify 
  local_node_hash = _genlocal.generate_tree_hash('/home/luca/sharednotes_dev/test_add/')

  agent = m.get_agent()
  remote_node_hash = agent.generate_tree_hash_oversftp('/home/notanamber/notes_dev/test_add/')

  assert local_node_hash == remote_node_hash
  print('Test executed succesfully')
