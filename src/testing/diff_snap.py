from iface import snap


def test_add_file():
  path = '/home/luca/test_diff'
  last_tree = {'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'source.txt': 'd41d8cd98f00b204e9800998ecf8427e', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1'}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}
  current_tree = {'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'source.txt': 'd41d8cd98f00b204e9800998ecf8427e', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1', 'zoncazonca.txt' : 'b1129bae3d2522579947416bd421b77c'}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}

  res = snap.diff_snap(last_tree, current_tree, path = path)
  return res

def test_add_folder():
  path = '/home/luca/test_diff'
  last_tree = {'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'source.txt': 'd41d8cd98f00b204e9800998ecf8427e', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1'}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}
  current_tree = {'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'source.txt': 'd41d8cd98f00b204e9800998ecf8427e', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1', 'zoncazonca' : {'1' : 'b1129bae3d2522579947416bd421b77c', '2' : 'f2de3bf4027e0b76ae78e10b03ab0c82'}}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}

  res = snap.diff_snap(last_tree, current_tree, path = path)
  return res

def test_remove_file():
  path = '/home/luca/test_diff'
  current_tree = {'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'source.txt': 'd41d8cd98f00b204e9800998ecf8427e', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1'}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}
  last_tree = {'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'source.txt': 'd41d8cd98f00b204e9800998ecf8427e', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1', 'zoncazonca.txt' : 'b1129bae3d2522579947416bd421b77c'}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}

  res = snap.diff_snap(last_tree, current_tree, path = path)
  return res

def test_remove_folder():
  path = '/home/luca/test_diff'
  current_tree = {'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'source.txt': 'd41d8cd98f00b204e9800998ecf8427e', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1'}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}
  last_tree = {'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'source.txt': 'd41d8cd98f00b204e9800998ecf8427e', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1', 'zoncazonca' : {'1' : 'b1129bae3d2522579947416bd421b77c', '2' : 'f2de3bf4027e0b76ae78e10b03ab0c82'}}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}

  res = snap.diff_snap(last_tree, current_tree, path = path)
  return res

def test_move_file():
  path = '/home/luca/test_diff'
  current_tree = {'source.txt': 'd41d8cd98f00b204e9800998ecf8427e','file1.txt': '0a4b9e726e7f6efad41b5015ba08240d','diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1', 'zoncazonca' : {'1' : 'b1129bae3d2522579947416bd421b77c', '2' : 'f2de3bf4027e0b76ae78e10b03ab0c82'}}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}
  last_tree = {'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1', 'zoncazonca' : {'1' : 'b1129bae3d2522579947416bd421b77c', '2' : 'f2de3bf4027e0b76ae78e10b03ab0c82','source.txt': 'd41d8cd98f00b204e9800998ecf8427e'}}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}

  res = snap.diff_snap(last_tree, current_tree, path = path)
  return res

def test_move_folder():
  path = '/home/luca/test_diff'
  current_tree = {'zoncazonca' : {'1' : 'b1129bae3d2522579947416bd421b77c', '2' : 'f2de3bf4027e0b76ae78e10b03ab0c82','source.txt': 'd41d8cd98f00b204e9800998ecf8427e'}, 'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1'}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}
  last_tree = {'file1.txt': '0a4b9e726e7f6efad41b5015ba08240d', 'diff.txt': '2ea73e06145254635c051de3232ce911', 'diff_cmd': 'f1da3af4027e0b76ae78e10b03ab0c82', 'folder': {'file1.txt': '163af31276c447901a7769a9645df394', 'folder1': {'zonca.txt': 'f89bd9df06590b8c12585e3dce5179c1', 'zoncazonca' : {'1' : 'b1129bae3d2522579947416bd421b77c', '2' : 'f2de3bf4027e0b76ae78e10b03ab0c82','source.txt': 'd41d8cd98f00b204e9800998ecf8427e'}}}, 'file2.txt': 'b1129bae3d2522579947416bd420a73b'}

  res = snap.diff_snap(last_tree, current_tree, path = path)
  return res


def test_some_transformation():
  '''
    changed zonca.txt and moved foder1 into a new created folder ff
  '''
  path = '/home/luca/test_diff/'
  last_tree = {
    "file1.txt": "0a4b9e726e7f6efad41b5015ba08240d",
    "source.txt": "d41d8cd98f00b204e9800998ecf8427e",
    "diff.txt": "2ea73e06145254635c051de3232ce911",
    "diff_cmd": "f1da3af4027e0b76ae78e10b03ab0c82",
    "folder": {
      "file1.txt": "163af31276c447901a7769a9645df394",
      "folder1": {
        "zonca.txt": "f89bd9df06590b8c12585e3dce5179c1"
      }
    },
    "file2.txt": "b1129bae3d2522579947416bd420a73b"
  }

  current_tree = {
    "file1.txt": "0a4b9e726e7f6efad41b5015ba08240d",
    "source.txt": "d41d8cd98f00b204e9800998ecf8427e",
    "diff.txt": "2ea73e06145254635c051de3232ce911",
    "diff_cmd": "f1da3af4027e0b76ae78e10b03ab0c82",
    "file2.txt": "b1129bae3d2522579947416bd420a73b",
    "ff": {
      "folder": {
        "file1.txt": "163af31276c447901a7769a9645df394",
        "folder1": {
          "zonca.txt": "2c50128790b60a0c89319f9cc8b31eb0"
          # "zonca.txt": "f89bd9df06590b8c12585e3dce5179c1"
        }
      }
    }
  }

  res = snap.diff_snap(last_tree, current_tree, path = path)
  return res
