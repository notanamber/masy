# from tools import parse_conf as _parse_conf
import time
import os
import subprocess
from pathlib import Path

from lib.repr.node import node_factory
from lib.snapshot.generate import local as _genlocal
from lib.snapshot import dump as _dump

from lib.sclient.agent import AdvancedSftpClientAgent

from lib.tree import treehash
from lib.tree import diff as _treediff
from lib.diff import fdiff
from lib.utils import path_utils as _path_utils
from lib.utils import node as _node_utils

from tools import colors

from iface import snap
# from iface import Node

DEFAULT_MANAGER_CONF = './conf/manager.conf'

class Manager():

  def __init__(self, task: int=0, local_path: str='', remote_path: str=''):
    if task:
      ## get conf from task table
      ## usually at ~/.syncdir.sync, a line is  a task 
      ## c00092bca01e2b580d84ac04b5dcab05 p2185521 w1693995892 STOPPED /home/luca/sharednotes/ notanamber@myvps:/home/notanamber/notes/
      pass
    else:
      if not local_path or not remote_path: 
        raise Exception('Please specify a local path and a remote path to sync')
      self.local_path = self.normalize_path(local_path)
      # self.remote_path = self.normalize_path(remote_path)
      self.init_remote_params(remote_path)

    # self.check_conf()

    # conf_file = conf_file or DEFAULT_MANAGER_CONF
    # self.conf = _parse_conf.read_conf(conf_file)

  def init_remote_params(self, remote_path):
    username, other = remote_path.split('@')
    hostname, remote_path = other.split(':')
    self.username = username
    self.hostname = hostname
    self.remote_path = self.normalize_path(remote_path)

  def check_init_conf(self):
    local_path = Path(self.local_path)
    if not local_path.exists():
      create = input('Do you want create the location %s? [Y/n] ' % (local_path))
      if create == 'n' or create == 'N':
        exit(0)
      print('Creating path: %s' % (local_path,))
      # if create == 'Y' or create == 'y'
      os.mkdir(local_path)
    else:
      if not local_path.is_dir():
        raise Exception('Local path is not a valid folder!')

  def normalize_path(self, path):
    # if path.endswith(os.sep): return path
    # return '%s%s' % (path, os.sep)
    return _path_utils.normalize_path(path)

  def mirror(self, dry_run=False):
    '''
      do an itial rsync 

      rsync -i -aPu --progress --out-format="%i ${GREEN}%n%L${ENDCOLOR} %''b" --log-file=$logfile -e ssh $otheropts $src $dest 

      rsync -i -aPu --progress --dry-run -e ssh notanamber@myvps:/home/notanamber/sharednotes_dev /home/luca/sharednotes_dev/

      Note:
        - if local_path don't exists it is created
        - if already exists can be a good idea to report it to user!
    '''

    local_path = self.local_path
    remote_path = self.remote_path

    username = self.username
    hostname = self.hostname

    dry_run_flag = ''
    if dry_run:
      dry_run_flag = ' --dry-run'

    cmd_str = f'rsync -i -aPu --progress{dry_run_flag} -e ssh {username}@{hostname}:{remote_path} {local_path}'

    cmd = cmd_str.split(' ')
    print(f'cmd: {cmd}')

    subprocess.run(cmd_str.split(' '))

  def compute_local_hash(self):
    local_path = self.local_path
    local_hash = _genlocal.generate_tree_hash(local_path)
    return local_hash

  def get_snapshot_path(self):
    local_path = self.local_path
    snapshot_dump_path = '%s%s%s%s' % (local_path, '.masy', os.sep, 'snapshot')
    snapshot_dump_path = self.normalize_path(snapshot_dump_path)
    return snapshot_dump_path

  def get_unmerged_path(self):
    local_path = self.local_path
    unmerged_path = '%s%s%s%s' % (local_path, '.masy', os.sep, 'unmerged')
    unmerged_path = self.normalize_path(unmerged_path)
    return unmerged_path

  def get_tree_diff_path(self, node):
    local_path = self.local_path
    tree_diff_path = '%s%s%s%s' % (local_path, '.masy', os.sep, 'diff')
    tree_diff_path = self.normalize_path(tree_diff_path)
    # node_path = node['path']
    # node_name = node['name']
    # return tree_diff_path
    return node['rel_path'].replace('.%s' % (os.sep), tree_diff_path)

  def get_tree_structure_diff_local_remote(self):
    local_path = self.local_path
    local_structure = treehash.generate_tree_structure(local_path)
    agent = self.get_agent()
    remote_structure = agent.generate_tree_structure_oversftp(self.remote_path)
    tree_structure_diff = _treediff.raw_structure_diff(local_structure, remote_structure, tree_a_tag='local', tree_b_tag='remote')
    return tree_structure_diff

  def dump_local_hash(self, hsh=None):
    # local_path = self.local_path
    # snapshot_dump_path = '%s%s%s%s' % (local_path, '.masy', os.sep, 'snapshot')
    if hsh is None:
      hsh = self.compute_local_hash()
    snapshot_dump_path = self.get_snapshot_path()
    try:
      os.makedirs(snapshot_dump_path)
    except FileExistsError:
      print(f'{snapshot_dump_path} already exists skip creation')
    _dump.dump_snapshot(hsh, path=snapshot_dump_path)

  def load_local_hash(self):
    snapshot_dump_path = self.get_snapshot_path()
    snapshot = _dump.load_snapshot(snapshot_dump_path)
    return snapshot

  def get_unmerged_local_path(self, node):
    node_path = node['rel_path']
    node_name = node['name']
    unmerged_local_file_path = '%s%s' % (node_path, node_name)
    unmerged_file_path = unmerged_local_file_path.replace('.%s' % (os.sep), self.get_unmerged_path())
    return unmerged_file_path

  def get_remote_mount_point(self, node):
    node_path = node['rel_path']
    remote_mount_point = node_path.replace('.%s' % (os.sep), self.remote_path)
    return remote_mount_point

  def get_local_mount_point(self, node):
    node_path = node['rel_path']
    local_mount_point = node_path.replace('.%s' % (os.sep), self.local_path)
    return local_mount_point

  def instance_of_node_to_sync(self, noded):
    '''
      i get a dictionary with some keys 
      and i return an instance of NodeToSync
    '''
    agent = self.get_agent()
    local_base_path = self.local_path
    remote_base_path = self.remote_path
    n = node_factory(noded, agent, local_base_path=local_base_path, remote_base_path=remote_base_path)
    return n

  def add_sync_to_list(self):
    home_dir = os.environ.get('HOME')
    home_dir = self.normalize_path(home_dir)
    file_name = '.masync.sync'
    local_path = self.local_path
    remote_path = self.remote_path
    last_action = int(time.time())
    try:
      with open('%s%s' % (home_dir,file_name), 'a') as f:
        line = f'{local_path} {remote_path} {last_action} INIT\n'
        f.write(line)
    except Exception as ss:
      raise Exception(f'can\' create file {home_dir}{file_name}')
    

  def init_sync(self):
    self.check_init_conf()
    self.mirror()
    ## maybe it is better to put all in dump_local_hash
    local_hash  = self.compute_local_hash()
    self.dump_local_hash(local_hash)
    self.add_sync_to_list()

  # def init_rh_agent(self):
  #   return RHAgent(self.hostname, self.username)

  def get_agent(self):
    try:
      return self.agent
    except: pass
    self.agent = AdvancedSftpClientAgent(self.hostname, self.username)
    return self.agent

  def get_local_snap_diff(self):
    '''
      `last_tree` is the last tree saved
      in file system after last sync
      `current_tree` is computed when method is called
      not `types` in these tree
    '''
    last_tree = self.load_local_hash()
    current_tree = self.compute_local_hash()
    local_snap_diff = snap.diff_snap(last_tree, current_tree)
    return local_snap_diff

  def get_remote_snap_diff(self):
    pass

  def store_fdiff(self, node, only_print=False):
    '''
      store in file diff path `.masy/diff` the tree with diffs
      file is containing the diff between local and remote
    '''
    LNode = node_factory(node, base_path=self.local_path)
    node_local_path = LNode.absolute_path()
    filea_block_tag = '%s %s (local)' % (node['name'], node['cur_hash'])
    fileb_block_tag = '%s %s (remote)' % (node['name'], node['remote_hash'])
    # rfile_buf = ''
    # if node.get('remote_file_buf'):
    rfile_buf = node.pop('remote_file_buf')
    node_diff_path = self.get_tree_diff_path(node)
    try:
      os.makedirs(node_diff_path)
    except FileExistsError:
      print(f'{node_diff_path} already exists skip creation')
      
    outfile = ''
    if not only_print:
      outfile = '%s%s' % (self.normalize_path(node_diff_path), node['name'])
    fdiff.print_diff(node_local_path, rfile_buf, remove_diff_letters_code=False, outfile=outfile, filea_block_tag=filea_block_tag, fileb_block_tag=fileb_block_tag)
    return node

  def store_unmerged_diff(self, nodes):
    '''
      node_path is the relative path
    '''
    unmerged_path = self.get_unmerged_path()
    # unmerged_path = Path(self.local_path)
    # if not local_path.exists():
    try:
      os.makedirs(unmerged_path)
    except FileExistsError:
      print(f'{unmerged_path} already exists skip creation of unmerged')
    
    ## generating tree diff 
    # tree_diff_path = self.get_tree_diff_path()
    todump = []

    for node in nodes:
      if node['cur_type'] == 'f':
        if node.get('remote_file_buf') is not None:
          n = self.store_fdiff(node)
        else:
          n = node
        todump.append(n)
        continue
      todump.append(node)

    _dump.dump_snapshot(todump, path=unmerged_path, dump_file_name='.unmerged.json.gz')


  def load_unmerged_diff(self):
    unmerged_path = self.get_unmerged_path()
    dump_file_name='.unmerged.json.gz'
    nodes = _dump.load_snapshot(unmerged_path, dump_file_name=dump_file_name)
    return nodes

  def get_rnode_status(self, node):
    '''
      we start from a local node
      gived a node check the remote status
         - node exists in remote
         - hash changed
    '''
    LNode = node_factory(node, base_path=self.local_path)
    RNode = node_factory(node, base_path=self.remote_path)
    absolute_remote_path = RNode.absolute_path()

    agent = self.get_agent()
    ## if node not exist in the local tree there is not last_type
    ## we use cur_type (is this correct?)
    node_type = node.get('last_type') or node['cur_type']
    if node_type == 'f':
      return agent.check_rfile_status(absolute_remote_path)
    ## node is a folder
    return agent.check_rfolder_status(absolute_remote_path)

  def copy_node_to_remote(self, node):
    '''
      use put of sftp client
      put(localpath, remotepath, callback=None, confirm=True)
    '''
    LNode = node_factory(node, base_path=self.local_path)
    RNode = node_factory(node, base_path=self.remote_path)

    absolute_local_path = LNode.absolute_path()
    absolute_remote_path = RNode.absolute_path()

    agent = self.get_agent()
    if node['cur_type'] == 'f':
      ## simply put 
      return agent.copy_file(absolute_local_path, absolute_remote_path)
    elif node['cur_type'] == 'd':
      #cur_hash contains the entire subtree to recreate on remote
      ## it is better to remove the root node if already exists on remote
      remote_mount_point = self.get_remote_mount_point(node)
      return agent.copy_dir(absolute_local_path, remote_mount_point)

  def remove_node_remotely(self, node):
    RNode = node_factory(node, base_path=self.remote_path)
    absolute_remote_path = RNode.absolute_path()

    agent = self.get_agent()
    if RNode.is_file():
      ## simply put 
      return agent.remove_file(absolute_remote_path)
    elif RNode.is_dir():
      #cur_hash contains the entire subtree to recreate on remote
      ## it is better to remove the root node if already exists on remote
      # remote_mount_point = self.get_remote_mount_point(node)
      return agent.remove_dir(absolute_remote_path)

  def show_conflicts(self):
    unmerged = self.load_unmerged_diff()
    for n, unode in enumerate(unmerged):
      i = n+1 
      LNode = node_factory(unode, base_path=self.local_path)
      absolute_local_path = colors.CYAN(LNode.absolute_path())
      name = LNode.name
      remote_hash = LNode.remote_hash
      node_type = LNode.get_type()
      node_action = LNode.get_action()
      local_hash = LNode.cur_hash
      last_hash = LNode.last_hash if LNode.last_hash is not None else 'File was not present'

      if remote_hash is None:
        remote_hash = colors.RED('File not present in remote')
      elif local_hash is None:
        local_hash = colors.RED('File not present local')

      print(f'{i}- path: \'{absolute_local_path}\' ({node_type} - {node_action}) \n  local: \'{last_hash}\' \u21E8 \'{local_hash}\',\
          \n  remote: \'{last_hash}\' \u21E8 \'{remote_hash}\'')

  def manual_solve_conflict(self, n, version_kept):
    i = n-1
    unmerged = self.load_unmerged_diff()
    node = unmerged[i]

    # precheck_res, precheck_msg = self.precheck_remote_unode(node)
    # if not precheck_res:
    #   print(precheck_msg)
    #   return

    conflicting, unode = self.check_remote_for_conflict(node)
    if conflicting:
      print(f'remote hash changed you...generate a new conflict')
      unmerged[i] = unode
      self.store_unmerged_diff(unmerged)
      return

    LNode = node_factory(node, base_path=self.local_path)
    RNode = node_factory(node, base_path=self.remote_path)
    absolute_local_path = colors.CYAN(LNode.absolute_path())
    absolute_remote_path = colors.CYAN(RNode.absolute_path())

    if version_kept == 'keep_local':
      # before copy check the remote
      ## copy node to remote
      print(f'Push local node: {absolute_local_path} to remote path: {absolute_remote_path}')
      self.remove_node_remotely(node)
      self.copy_node_to_remote(node)
      new_local_tree = self.update_local_hash('changed', node)
      ## save new_local_tree
      print(f'Dump new tree {new_local_tree}')
      self.dump_local_hash(new_local_tree)
      ## remove conflict and save
      unmerged.pop(i)
      self.store_unmerged_diff(unmerged)
    elif version_keps == 'kepp_remote':
      ## copy node from remote to local
      pass
    else:
      # raise Exception
      pass

  def check_remote_for_conflict(self, node):
    '''
      i receive a node i'll check on remote the hash
      for verify if equal to `last_hash` and eventually i generate a conflict
      i return if there is a conflict and the unmerged node
    '''

    conflicting = False
    rstatus = self.get_rnode_status(node)
    rfile_buf, rhash, rexists = map(rstatus.get, ['iobuffer', 'hash', 'exists'])
    print(f'Remote status file exists {rexists}, {rhash}')




    remote_hash = node.get('remote_hash')
    if remote_hash is None and rexists:
      # if node is added in local tree, 
      # but a copy appears in remote tree
      # we don't know before, `remote_hash` don't exists
      # it is possible remote_file_buf exists
      conflicting = True
      unode = node.copy()
      unode['last_hash'] = None
      unode['remote_hash'] = rhash
      if rfile_buf: unode['remote_file_buf'] = rfile_buf
      return conflicting, unode

    if rhash == remote_hash: 
      return conflicting, {}

    conflicting = True
    unode = node.copy()
    # (cur_hash, cur_buf) = _genlocal.generate_file_hash(self.get_absolute_local_path(node))
    # node['cur_hash'] = cur_hash
    unode['last_hash'] = node['remote_hash']
    unode['remote_hash'] = rhash
    unode['remote_file_buf'] = rfile_buf

    return conflicting, unode

  def mark_conflicting_node_as_solved(self, node):
    name = node['name']
    path = node['rel_path']

    unmerged = self.load_unmerged_diff()

    found = {}
    for n, unode in enumerate(unmerged):
      if unode['name'] == name and unode['rel_path'] == path:
        found = unode
        break
    else: 
      n = -1

    if n == -1: 
      return 'No node conflicting'

    remote_hash = found['remote_hash']
    ## if you mark the node as conflict solved
    ## you have incorporated the `remote_hash`
    ## you must verify this is the actual hash in the server side

    conflicting, unode = self.check_remote_for_conflict(node)
    if conflicting:
      print(f'remote hash changed you...generate a new conflict')
      unmerged[n] = unode
      self.store_unmerged_diff(unmerged)
      ## store diff in diffs path
      # self.store_fdiff(unode)
      return

    ## found the node in unmerged path
    # unmerged_local_path = self.get_unmerged_local_path(node)
    # absolute_remote_path = self.get_absolute_remote_path(node)
    # copy node from the unmerged path to remote -> this is 
    print(f'force local node: {unmerged_local_path} to remote path: {absolute_remote_path}')
    self.copy_node_to_remote(node)

  def sync_added(self, added=False, dry_run=False):
    if dry_run: return []

    if added is None:
      local_snap_diff = self.get_local_snap_diff()
      added = local_snap_diff.get('added') or []

    dones = []
    unmerged = self.load_unmerged_diff()

    for node in added:
      node_name = node['name']
      last_hash = node.get('last_hash')
      if last_hash is not None:
        raise Exception('node added has a last hash!!')
      print(f'Checking local {node_name}')

      # rstatus = self.get_rnode_status(node)
      # rfile_buf, rhash, rexists = map(rstatus.get, ['iobuffer', 'hash', 'exists'])
      conflicting, unode = self.check_remote_for_conflict(node)
      if conflicting:
        print('Remote node appeared in remote tree but in the local tree there was not, add it to unmerged')
        unmerged.append(unode)
        continue

      self.copy_node_to_remote(node)
      ## mark node as synched with remote, 
      dones.append(node)

    if unmerged:
      self.store_unmerged_diff(unmerged)

    return dones


  def sync_changed(self, changes=None, dry_run=False):
    if dry_run:
      return []

    if changes is None:
      local_snap_diff = self.get_local_snap_diff()
      changes = local_snap_diff.get('changed') or []

    unmerged = []
    dones = []

    for node in changes:
      node_name = node['name']
      last_hash = node['last_hash']
      print(f'Checking local {node_name}')

      rstatus = self.get_rnode_status(node)
      rfile_buf, rhash, rexists = map(rstatus.get, ['iobuffer', 'hash', 'exists'])
      node['remote_hash'] = rhash
      node['remote_file_buf'] = rfile_buf

      if not rexists or last_hash != rhash:
        print(f'Put node {node_name} in unmerged, node not exists in remote or is changed from the last local snapshot')
        unmerged.append(node)
        continue

      dones.added(node)

      print(f'You can proceed to push {node_name} file it is not changed from the last version')
      self.copy_node_to_remote(node)

    if unmerged:
      self.store_unmerged_diff(unmerged)

    return dones

  def sync_removed(self, removed=None, dry_run=False):
    if dry_run: return []

    if removed is None:
      local_snap_diff = self.get_local_snap_diff()
      removed = local_snap_diff.get('removed') or []

    unmerged = []
    dones = []

    for node in removed:
      node_name = node['name']
      last_hash = node['last_hash']
      print(f'Checking local {node_name}')

      rstatus = self.get_rnode_status(node)
      rfile_buf, rhash, rexists = map(rstatus.get, ['iobuffer', 'hash', 'exists'])
      node['remote_hash'] = rhash
      node['remote_file_buf'] = rfile_buf

      if rexists and last_hash != rhash:
        print(f'Put node {node_name} in unmerged, node exists in remote but hash diff from know last_hash snapshot')
        unmerged.append(node)
        continue

      if not rexists:
        print(f'{node_name} not still exists in remote')
      else:
        print(f'You can proceed to remove {node_name} file it is not changed from the last version')
        self.remove_node_remotely(node)

      dones.append(node)


    if unmerged:
      self.store_unmerged_diff(unmerged)

    return dones

  def compute_local_node_hash(self, n):
    ## we must compute n['cur_hash']
    LNode = node_factory(n, base_path=self.local_path)
    absolute_local_path = LNode.absolute_path()
    if n['last_type'] == 'd':
      cur_hash = _genlocal.generate_tree_hash(absolute_local_path)
    else:
      cur_hash = _genlocal.generate_file_hash(absolute_local_path)

    return cur_hash

  def copy_node_from_remote(self, n):
    a = self.get_agent()
    last_type = n['last_type']
    # absolute_remote_path = self.get_absolute_remote_path(n)
    RNode = node_factory(n, base_path=self.remote_path)
    absolute_remote_path = RNode.absolute_path()

    if last_type == 'd':
      local_mount_point = self.get_local_mount_point(n)
      return a.get_dir(absolute_remote_path, local_mount_point)

    LNode = node_factory(n, base_path=self.local_path)
    absolute_local_path = LNode.absolute_path()
    return a.get_file(absolute_remote_path, absolute_local_path)


  def sync_from_remote(self, nodes, dry_run=False):
    if dry_run: return []
    dones = []
    for n in nodes:
      self.copy_node_from_remote(n)
      cur_hash = self.compute_local_node_hash(n)
      n['cur_hash'] = cur_hash
      dones.append(n)

    return dones


  def get_candidates_for_local_deletion_and_addition(self):
    ## candidates_for_local_deletion  -> nodes that there are on local but not in remote
    ## candidates_for_local_addition  -> nodes that there are NOT on local but only in remote
    # make a remote call to check the structure diff
    tree_structure_diff = self.get_tree_structure_diff_local_remote()

    candidates_for_local_deletion = []
    candidates_for_local_addition = []

    for node,  noded in tree_structure_diff.items():
      is_in_remote_tree = noded.get('remote')
      is_in_local_tree = noded.get('local')

      rindex = node.rindex(os.sep)

      name = node[rindex+1:]
      rel_path = node[:rindex]
      # rtype = noded.get('remote_type') or noded.get('local_type')

      if is_in_local_tree and not is_in_remote_tree:
        candidates_for_local_deletion.append({
          'name' : name,
          'rel_path' : rel_path,
          'last_type' : noded['local_type'],
        })

      if is_in_remote_tree and not is_in_local_tree:
        candidates_for_local_addition.append({
          'name' : name,
          'rel_path' : rel_path,
          'last_type' : noded['remote_type'],
        })

    return {
        'deletion' : candidates_for_local_deletion,
        'addition' : candidates_for_local_addition,
        }

  def sync(self, dry_run=False):
    '''
      sync:
        - calculate local diff
        - collect necessary action to sync server with this node
        - do a remote diff and compare with the last version in this node
              if there are not differencies simply do necessary actions to sync
              if there is differences put the file diff in a specific folder
    '''
    # check if there are unmerged files
    # unmerged are unsynchronized nodes due to conflict

    unmerged_path = self.get_unmerged_path()
    if os.listdir(unmerged_path):
      if self.load_unmerged_diff():
        print('You have this unmerged file(s), if you want to proceed with sync, fix it:\n')
        self.show_conflicts()
        return


    candidates = self.get_candidates_for_local_deletion_and_addition()

    candidates_for_local_deletion = candidates['deletion']
    candidates_for_local_addition = candidates['addition']

    # candidates_for_local_deletion=True    ->   if `added` in local_snap_diff -> push it to remote, else remove in local
    # candidates_for_local_addition=True    ->   if `cancelled` in local_snap_diff -> push cancellation to remote, else add in local

    local_snap_diff = self.get_local_snap_diff()
    local_tree = self.load_local_hash()


    changes = local_snap_diff.get('changed') or []
    changed_done = self.sync_changed(changes=changes, dry_run=dry_run)

    for node in changed_done:
      local_tree = self.update_local_hash('changed', node, tree=local_tree)

    added = local_snap_diff.get('added') or []

    local_deletion = []
    for cdel in candidates_for_local_deletion:
      index = _node_utils.find_index_of_node(cdel, added)
      # i must cancel the candidates in local
      # if i can't found it in `local added`
      # or if candidate is a `dir` and that `dir`
      # is not in path of some node added
      if index == -1 and cdel['last_type'] == 'f':
        local_deletion.append(cdel)
        continue

    added_done = self.sync_added(added=added, dry_run=dry_run)

    for node in added_done:
      local_tree = self.update_local_hash('add', node, tree=local_tree)

    removed = local_snap_diff.get('removed') or []

    remote_addition = []
    for cadd in candidates_for_local_addition:
      index = _node_utils.find_index_of_node(cadd, removed)
      if index == -1: remote_addition.append(cadd)

    # local_added = self.sync_added(added=added, dry_run=dry_run)
    added_from_remote_done = self.sync_from_remote(nodes=remote_addition, dry_run=dry_run)

    for node in added_done + added_from_remote_done:
      local_tree = self.update_local_hash('add', node, tree=local_tree)

    removed_done = self.sync_removed(removed=removed, dry_run=dry_run)

    for node in removed_done:
      local_tree = self.update_local_hash('removed', node, tree=local_tree)

    ## dump new tree
    if not dry_run:
      self.dump_local_hash(hsh=local_tree)

    return local_tree, local_deletion, remote_addition

  def update_local_hash(self,action, node_changed, tree=None):
    if not tree:
      tree = self.load_local_hash()

    rel_path_list = node_changed['rel_path'].split(os.sep)
    rel_path_list = [r for r in rel_path_list if r]
    node_name = node_changed['name']
    cur_hash = node_changed.get('cur_hash')
    subtree = tree
    for kpath in rel_path_list:
      if kpath == '.': continue
      subtree = subtree[kpath]

    if action == 'remove':
      subtree.pop(node_name, None)
      return tree

    if action == 'add':
      subtree[node_name] = {}

    try:
      subtree[node_name].update(cur_hash) 
    except Exception as e:
      print(f'Exception: {e}, {cur_hash}')
      ## if key is an hash pass and modify the key
      subtree[node_name] = cur_hash

    return tree
    
  
