import os 

from lib.snapshot import diff as _diff
from lib.snapshot import dump as _dump
from lib.snapshot.generate import local as _genlocal

from tools import parse_conf as _pconf

def dump_snap(root_tree: str):
  snapshot = _genlocal.generate_tree_hash(root_tree)

  conf = _pconf.read_conf('./conf/snapshot.conf')

  snap_path = conf.get('dump_path')
  filename = conf.get('dump_filename')

  _dump.dump_snapshot(snapshot, path = snap_path, dump_file_name=filename)

def decode_snap():
  conf = _pconf.read_conf('./conf/snapshot.conf')

  snap_path = conf.get('dump_path')
  filename = conf.get('dump_filename')

  return _dump.decode_snapshot(path=snap_path, dump_file_name = filename)

# def recursive_diff_snap(last_tree, current_tree, path='./', bres={}):
#   '''
#     XXX: use key names convention specified in lib/repr/node.py
#     snapshot is a tree represented by dictionary {k, val} when k can be folder or file name
#     and val can be a dictionary or an hash
#
#     @param                 last_tree                 snapshot computed at time t0
#     @param                 current_tree              snapshot computed at time t1
#   '''
#   removed = bres.setdefault('removed', [])
#   changed = bres.setdefault('changed', [])
#   added = bres.setdefault('added', [])
#
#   keys_added = set(current_tree.keys()) - set(last_tree.keys())
#   ## name last is a dir
#   if keys_added:
#     for key_added in keys_added:
#       print(f'file {path}{key_added} subtree added in current snapshot')
#       item = {
#         'name' : key_added,
#         'rel_path' : '%s' % (path,),
#         'cur_type' : 'd' if isinstance(current_tree[key_added], dict) else 'f',
#         'cur_hash' : current_tree[key_added],
#       }
#       added.append(item)
#
#   for name_last, hsh_last in last_tree.items():
#     hsh_current = current_tree.get(name_last)
#
#     last_type = 'd' if isinstance(hsh_last, dict) else 'f'
#
#     item = {
#         'name' : name_last, 
#         'rel_path' : path,
#         # 'last_hash' : hsh_last,
#         }
#
#     if hsh_current is None:
#       ## the name in not founded in current snapshot - Subtree removed (or moved)
#       print(f'{path}{name_last} present in last_tree but removed in current_tree, subtree {name_last} removed in current_tree')
#       item.update({
#         'last_type' : last_type,
#         'last_hash' : hsh_last,
#         })
#       removed.append(item)
#       continue
#
#     current_type = 'd' if isinstance(hsh_current, dict) else 'f'
#
#     if type(hsh_last) != type(hsh_current):
#       print(f'{path}{name_last} changed his type in {current_type}')
#       item.update({
#         'last_type' : last_type,
#         'cur_type' : current_type,
#       })
#       changed.append(item)
#       continue
#
#     # current node is a file
#     if isinstance(hsh_last, str):
#       if hsh_last != hsh_current:
#         print(f'file {path}{name_last} changed his hash')
#         item.update({
#           'last_type' : last_type,
#           'cur_type' : current_type,
#           'cur_hash' : hsh_current,
#           'last_hash' : hsh_last,
#         })
#         changed.append(item)
#       continue
#
#     # current node is dir
#     if (hsh_last == hsh_current):
#       print(f'file {path}{name_last} subtree unchanged ')
#       continue
#
#     print(f'file {path}{name_last} subtree changed in current snapshot')
#
#     recursive_diff_snap(hsh_last, hsh_current, path='%s%s/' % (path,name_last), bres = {
#       'removed' : removed,
#       'added' :  added,
#       'changed' : changed,
#       })

# def diff_snap(last_tree, current_tree, path='./'):
#   res = {}
#   if not path.endswith(os.sep): path = path + os.sep
#   recursive_diff_snap(last_tree, current_tree, path=path, bres=res)
#   # return res
#   ## Managing moved files is too difficult - for now skip this step
#   # compute moved could save bandwidth
#   res['moved'] = []
#   for n, r in enumerate(res['removed']):
#     if r.get('hash') is None:
#       continue
#     found, path = find_subtree(current_tree, {
#       r['name'] : r['hash'],
#       })
#     if not found: continue
#     res['moved'].append({
#       'name' : r['name'], 
#       'old_path' : r['path'],
#       'new_path' : path,
#       'hash' : r['hash'],
#       })
#
#     del res['removed'][n]
#   
#   return res

def diff_snap(last_tree, current_tree, path='./'):
  tree_diff = _diff.raw_diff(last_tree, current_tree, tree_a_tag='last', tree_b_tag='cur', path=path)
  ##  we need 3 keys
  ##  'removed', 'changed', 'added'
  removed = []
  added = []
  changed = []

  for node in tree_diff.keys():
    ## node must have a structure as reported in lib/repr/node.py
    last_index = node.rfind(os.sep)
    node_name = node[last_index+1:]
    node_rel_path = node[:last_index+1]

    noded = tree_diff[node]

    n = {
        'name' : node_name,
        'rel_path': node_rel_path,
        }

    if noded.get('last_type') is not None: 
      n['last_type'] = noded['last_type']

    if noded.get('cur_type') is not None: 
      n['cur_type'] = noded['cur_type']

    if noded.get('last_hash') is not None: 
      n['last_hash'] = noded['last_hash']

    if noded.get('cur_hash') is not None: 
      n['cur_hash'] = noded['cur_hash']

    if not noded.get('cur') and noded.get('last'):
      removed.append(n)

    if not noded.get('last') and noded.get('cur'):
      added.append(n)

    if noded.get('last') and noded.get('cur'):
      changed.append(n)

  return {
      'removed' : removed,
      'changed' : changed,
      'added' : added,
      }


def find_subtree(snapshot, subtree, path='./'):
  '''
    @param     subtree              dict
    find the exact subtree in snapshot, subtree can be 
      - a node representing a file
      - a node representing a folder
  '''
  subtree_key  = list(subtree.keys())[0]
  subtree_val = list(subtree.values())[0]

  for snap_key, snap_val in snapshot.items():
    if snap_key != subtree_key:
      if isinstance(snap_val, dict):
        path = '%s%s/' % (path, snap_key)
        return find_subtree(snap_val, subtree, path)
    else:
      if snap_val == subtree_val:
        return (True, path)
        print(f'subtree found in snapshot in path {path}')
  return (False, '')

