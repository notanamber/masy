#!/usr/bin/env python

if __name__ == '__main__':
  print('python masync script: #!/usr/bin/env python ')

  ## suppose last tree is

  last_local_tree_hash = {'taglioCapelli4.jpg': '5af62402ec7716e8d729b0683061f0c4', 'taglioCapelli5.jpg': '9c42961af589a279c4c828925291153b', 'pino.txt': 'd41d8cd98f00b204e9800998ecf8427e', 'taglioCapelli6.jpg': '4059905eab817c33ee48f912af80fdb7', 'spartiti_sepu': {'IMG_20220626_081839.jpg': '3c14e508124c928d59b393a571e2f751', 'IMG_20220626_081951.jpg': 'd484638ac09cbe40f8753de3f1b3c4a6'}, 'preferiti.txt': 'af45981ef2be534dbb37f96833d3fd04'}

  from lib.snapshot.generate import local as _genlocal
  from iface import snap as _snap
  from tools import pretty_print as _pretty_print 
  local_tree_hash = _genlocal.generate_tree_hash('/home/luca/sharednotes_dev/')
  diff = {}
  _snap.diff_snap(last_local_tree_hash, local_tree_hash, bres=diff)

  processed = {}

  processed['added'] = diff['added']
  processed['changed'] = diff['changed']

  removed = diff.get('removed') or []

  for item in removed:
    subtree = {
      item['name']: item['hash']
    }

    (found, path) = _snap.findsubtree(local_tree_hash, subtree)
    if not found: 
      cancelled = processed.setdefault('cancelled', [])
      cancelled.append(item)
      continue

    moved = processed.setdefault('moved', [])

    item['dest_path'] = path
    moved.append(item)

  _pretty_print.pp(processed)
