import io

from pathlib import Path
import difflib

def is_buf(f):
  if isinstance(f, io.BytesIO):
    return True
  if isinstance(f, io.StringIO):
    return True
  return False

def get_lines(file):
  if not file: return []
  if is_buf(file):
    return file.readlines()
  with open(file, 'r') as f:
    lines = f.readlines()
  return lines

def compute_diff(filea, fileb):
  '''
  file*      can be  a path or a buffer
  '''
  try:
    alines = get_lines(filea)
  except:
    print('alines error: file could be a binary')
    alines = ['binary content ---\n']
  try:
    blines = get_lines(fileb)
  except UnicodeDecodeError as e:
    print('blines error: file could be a binary')
    blines = ['--- binary content\n']

  differ = difflib.Differ()
  res_diff = list(differ.compare(alines, blines))
  return res_diff

def get_file_block_tag(file, file_block_tag):
  if is_buf(file):
    assert file_block_tag
    return file_block_tag
  if file_block_tag: 
    return file_block_tag
  ## file is a path
  file_obj = Path(file)
  file_block_tag = file_obj.name
  return file_block_tag

def format_diff(filea, fileb, remove_diff_letters_code=False, filea_block_tag='', fileb_block_tag=''):
  '''
    "- "    line unique to file1
    "+ "    line unique to file2
    "  "    line common to both files
    "? "    line not present in either input file -> to skip it
  '''

  filea_block_tag = get_file_block_tag(filea, filea_block_tag)
  fileb_block_tag = get_file_block_tag(fileb, fileb_block_tag)

  filea_group_wrap_tmpl = '<<<<<<< '+ filea_block_tag + '\n%s=======\n'
  fileb_group_wrap_tmpl = '>>>>>>> ' + fileb_block_tag + '\n%s=======\n'


  lines_diff = compute_diff(filea, fileb)
  list_blocks = []

  '''
    list blocks contains object like this
    {
    'block_source' : `filea_block_tag|fileb_block_tag|common`
    'block_lines' : `lines of that block`
    }
  '''

  for line in lines_diff:
    last_block_index = len(list_blocks)

    if line.startswith('- '):
      ## line in filea
      block_source = filea_block_tag
      wrap_tmpl = filea_group_wrap_tmpl
    elif line.startswith('+ '):
      ## line in fileb
      block_source = fileb_block_tag
      wrap_tmpl = fileb_group_wrap_tmpl
    elif line.startswith('? '):
      continue
    elif line.startswith('  '):
      ## common line for both files
      block_source = 'common'
      wrap_tmpl = ''

    if remove_diff_letters_code:
      line = line[2:]

    if not last_block_index:
      # add a block
      list_blocks.append({
        'block_source' : block_source,
        'block_lines' : [line],
        'block_wrap_tmpl' : wrap_tmpl,
      })
      continue

    last_block = list_blocks[last_block_index-1]

    if last_block['block_source'] != block_source:
      list_blocks.append({
        'block_source' : block_source,
        'block_lines' : [line],
        'block_wrap_tmpl' : wrap_tmpl,
      })
      continue

    ## block already exists, append the line
    last_block['block_lines'].append(line)

  return list_blocks

def print_diff(filea_path, fileb_path, remove_diff_letters_code=False, outfile='', filea_block_tag='', fileb_block_tag=''):
  formatted = format_diff(filea_path, fileb_path, remove_diff_letters_code=remove_diff_letters_code, filea_block_tag=filea_block_tag, fileb_block_tag=fileb_block_tag)

  out_str = ''

  for n,f in enumerate(formatted):
    is_last  = True if n == len(formatted) -1 else False

    source = f['block_source']
    lines = f['block_lines']

    wrap_tmpl = f['block_wrap_tmpl']
    if wrap_tmpl:
      if is_last: 
        ## strip the last \n in template
        tmpl = wrap_tmpl[:-1]
      else:
        tmpl = wrap_tmpl

      out_str += tmpl % (''.join(lines),)
      continue

    out_str += ''.join(lines)

  print(out_str)
  if not outfile: return

  with open(outfile, 'w') as o:
    o.write(out_str)
  return 


