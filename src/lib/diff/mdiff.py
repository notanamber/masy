# import hashlib 

def compute_diff(path_filea, path_fileb):
  '''
    it returns binary diff line per line of two files,
    it is basically a list that contains:
      - blocks (lists) with the same lines
      - blocks of diffs, item with two lists 

    [[['hhhh\n', 'iiii\n', 'jjjj\n', 'eee\n', '\n'], ['cio cio cio cio cio ciao\n', ' cj cjcj\n', 'llll\n', 'jjjj\n', 'eee\n']], ['\n', '\n', '\n', '\n']]
                                              differente lines                                                                           same lines

  '''
  with open(path_filea, 'r') as fa:
    alines = fa.readlines()
  with open(path_fileb, 'r') as fb:
    blines = fb.readlines()

  diff_block = []
  same_block = []

  out = []
  ### compute
  while (alines or blines):
    aline = alines.pop(0) if alines else ''
    bline = blines.pop(0) if blines else ''

    if aline == bline:
      if diff_block: 
        out.append(diff_block)
        diff_block = []

      if same_block: same_block.append(aline)
      else: same_block = [aline]
      
      continue
    
    ## aline != bline
    if same_block:
      out.append(same_block)
      same_block = []

    if diff_block: 
      diff_block[0].append(aline)
      diff_block[1].append(bline)
    else:
      diff_block = [[aline], [bline]]


  if diff_block: out.append(diff_block)
  if same_block: out.append(same_block)

  return out

def format_diff(path_filea, path_fileb):
  ldiff = compute_diff(path_filea, path_fileb)

  tdiff = ''

  astart = '<<<<<<< %s\n' % (path_filea,)
  sep = '======='
  bstart = '>>>>>>> %s\n' % (path_fileb,)

  for block in ldiff:
    islast_block = True if block == ldiff[-1] else False

    if isinstance(block[0], list):
      ja = ''.join(block[0])
      tdiff += astart + ja + sep + '\n'
      jb = ''.join(block[1])
      tdiff += bstart + jb + sep
      if not islast_block:
        tdiff += '\n'
      continue

    if block[0] == '\n' and tdiff.endswith('%s\n' % (sep,)):
      block = block[1:]

    tdiff += ''.join(block)

  print(tdiff)
  return tdiff

def merge_diff(txt_diff, outfile):
  pass
