import os

from lib.repr import node as _node_repr


def find_index_of_node(n: dict, l: list, keys: list = []) -> int:
  '''
    n   a dict representing a node (refer to lib/repr/node.py)
    l   list where to find

    search node n in a list comparing all keys of n
    or the keys you pass, search only the first occurrence
  '''


  keys_to_check = keys if keys else n.keys()
  check_values = list(map(n.get, keys_to_check))

  for i, node in enumerate(l):
    if check_values == list(map(node.get, keys_to_check)):
      break

  else: i=-1

  return i

def node_in_path(n: dict, l: list):
  """
    it returns True if a node representing a dir
    is in at least one node present in l
  """
  pass


def node_from_path(base_path, absolute_path):
  """
  returning a TreeNode starting from a base_path and absolute_path
  example for local node (but you can same form for a remote)
  `node_from_path("/home/luca/sharednotes_dev",
                        "/home/luca/sharednotes_dev/spartiti_sepu/added_local/added_local_file.txt")`
  """
  if absolute_path.find(base_path) == -1:
    raise ("base path must be contained in absolute_path")

  last_sep_index = absolute_path.rindex(os.sep)
  abs_path = absolute_path[:last_sep_index + 1]
  relative_path = abs_path.replace(base_path, '.%s' % (os.sep))
  name = absolute_path.rsplit(os.sep)[-1]

  return _node_repr.TreeNode(name, relative_path, base_path)
