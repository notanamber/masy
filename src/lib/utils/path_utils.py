import os

def normalize_path(path):
  if path.endswith(os.sep): return path
  return '%s%s' % (path, os.sep)

def check_isdir(path: str):
  if not os.path.isdir(path):
    raise Exception('Provide a valid folder to start the hashing')

  if not path.endswith(os.path.sep):
    path = path + os.path.sep
  return path
