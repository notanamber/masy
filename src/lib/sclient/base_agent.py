import paramiko
from paramiko import SSHConfig

HOSTS_KEYS_PATH = '/home/luca/.ssh/known_hosts'
PKEY_PATH = '/home/luca/.ssh/notanamber_rsa'

class BaseAgent():
  '''
    Basic ssh connection layer
  '''

  def __init__(self, hostname, username):
    self.hostname = hostname
    self.username = username
    self.pkey = paramiko.RSAKey.from_private_key_file(PKEY_PATH)
    self.client = paramiko.SSHClient()
    self.client.load_host_keys(filename=HOSTS_KEYS_PATH)

  def get_hostname_from_sshconfig(self):
    ssh_config_path = "/home/luca/.ssh/config"
    ssh_host = self.hostname
    config = SSHConfig()
    config_file = open(ssh_config_path)
    config.parse(config_file)
    readed = config.lookup(ssh_host)
    return readed['hostname']

  def connect(self):
    '''
      this method generates the underlying `Trasport` 
    '''
    hostname = self.get_hostname_from_sshconfig()
    self.client.connect(hostname=hostname, username=self.username, pkey=self.pkey, look_for_keys=False)

  def close(self):
    self.client.close()
