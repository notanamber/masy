import time

import os
import stat
import errno

import asyncio
import hashlib
import io
import pickle

import functools

from lib.utils import path_utils as _path_utils
from lib.sclient.base_agent import BaseAgent
from lib.snapshot import key as _skey


class AdvancedSftpClientAgent(BaseAgent):
  '''
    it give to you a sftp client for using to compute remote hash
    and basic ftp command to synchronize local and remote
  '''

  def __init__(self, hostname, username):
    BaseAgent.__init__(self, hostname, username)
    self.sftpc = None

  def get_sftp_client(self):
    """ returning the sftp client to execute some task """
    if self.sftpc:
      return self.sftpc
    if not self.client.get_transport():
      print('Transport not present proceed to create it... ')
      self.connect()
      print('--> OK!')
    self.sftpc = self.client.open_sftp()
    return self.sftpc

  def rpath_attr(self, path='.'):
    '''
      return remote attributes of a path
      what happens if don't exists? it raise
    '''

    sftpc = self.get_sftp_client()
    attr = sftpc.lstat(path)
    return attr

  def remove_file(self, remote_file_path):
    '''
      @param       remote_file_path        absolute path of file to remove, must exists
    '''
    sftpc = self.get_sftp_client()
    print(f'Remove: {remote_file_path}')
    return sftpc.remove(remote_file_path)

  def remove_dir(self, absolute_path, dry_run=False):
    '''
      Here we assume that absolute_path on remote can be not empty
      # XXX: the ftp client can remove only empty folder
      test it
    '''
    sftpc = self.get_sftp_client()

    def remove_dir_recursive(path):
      # if not path.endswith(os.sep): path = '%s%s' % (path, os.sep)
      path = _path_utils.normalize_path(path)
      print(f'Path: {path}')
      items = sftpc.listdir_attr(path)
      if not items:
        # path is an empty directory you can procede to delete it
        print(f'Path: {path} empty removing')
        if not dry_run:
          sftpc.rmdir(path)
        return

      for item in items:
        absolute_item_path = path + item.filename
        print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item.filename, stat.S_ISDIR(item.st_mode)))
        if not stat.S_ISDIR(item.st_mode):
          # remove file
          print(f'Removing file {absolute_item_path}')
          if not dry_run:
           sftpc.remove(absolute_item_path)
          continue
        remove_dir_recursive(absolute_item_path)

      print(f'Path: {path} empty removing')
      if not dry_run:
        sftpc.rmdir(path)

    return remove_dir_recursive(absolute_path)

  def get_file(self, remotepath, localpath, callback=None):
    '''
      use get command to fetch a file from remote and put in local tree
    '''
    def def_cback(size, filesize):
      print(f'{localpath} copied to {remotepath}, success, ({filesize})')

    callback = callback if callback else def_cback
    client = self.get_sftp_client()
    client.get(remotepath, localpath, callback=callback)
    return 

  def get_dir(self, absolute_remote_path, local_mount_point, dry_run=False):
    sftpc = self.get_sftp_client()

    def get_dir_recursive(remote_path, local_path, dry_run=False):
      local_path = _path_utils.normalize_path(local_path)
      remote_path = _path_utils.normalize_path(remote_path)

      # if os.path.isdir(local_path):
      # items = sftpc.listdir_attr(path)

      items = sftpc.listdir_attr(remote_path)

      if not items:
        return

      # first you must create the folder so order 

      for item in items:
        absolute_item_path = local_path + item.filename
        remote_item_path = remote_path + item.filename
        # print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item.filename, stat.S_ISDIR(item.st_mode)))
        # if not os.path.isdir(absolute_item_path):
        if not stat.S_ISDIR(item.st_mode):
          ## we assume the folder already exists
          print(f'Get file {absolute_item_path} from remote {remote_item_path}')
          if not dry_run: sftpc.get(remote_item_path, absolute_item_path, callback=None)
          continue
        ## create folder on local with mkdir
        print(f'Creating dir {absolute_item_path} on remote is {remote_item_path}')
        if not dry_run: 
          os.mkdir(remote_item_path)
        get_dir_recursive(absolute_item_path, remote_item_path, dry_run=dry_run)

    remote_path_split = [s for s in absolute_remote_path.split(os.sep) if s]
    node_name = remote_path_split[-1]
    local_root_path = _path_utils.normalize_path(local_mount_point) + node_name
    print(f'Creating mount point in local : {local_root_path}')
    if not dry_run: 
      try:
        # create in local node_name folder
        os.mkdir(local_root_path)
      except IOError as e:
        print(f'Failure creating {local_root_path}: detail {e}')

    return get_dir_recursive(absolute_remote_path, local_root_path, dry_run=dry_run)

  def copy_file(self, localpath, remotepath, callback=None, confirm=False):
    '''
      put(localpath, remotepath, callback=None, confirm=True)
      remotepath must contains the name of file you want to copy
    '''
    def def_cback(size, filesize):
      print(f'{localpath} copied to {remotepath}, success, ({filesize})')

    callback = callback if callback else def_cback
    client = self.get_sftp_client()
    client.put(localpath, remotepath, callback=callback, confirm=confirm)
    return 

  def copy_dir(self,absolute_local_path, remote_mount_point, dry_run=False):
    '''
       copy recursivly a directory and its content on remote
       XXX IF FOLDER ALREADY EXISTS IT FAILS WITH IOError
       @param        absolute_local_path            root path on local file system
       @param        remote_mount_point             is a folder on the remote, the mount point (must exists)
    '''

    sftpc = self.get_sftp_client()

    def copy_dir_recursive(local_path, remote_path, dry_run=False):
      local_path = _path_utils.normalize_path(local_path)
      remote_path = _path_utils.normalize_path(remote_path)

      # if os.path.isdir(local_path):
      # items = sftpc.listdir_attr(path)
      items = os.listdir(local_path)

      if not items:
        print(f'Creating empty remote folder: {remote_path}')
        try:
          if not dry_run: sftpc.mkdir(remote_path)
        except IOError as e:
          print(f'Mount point already exists {remote_path}: detail {e}')
        return 


      # first you must create the folder so order 

      for item in items:
        absolute_item_path = local_path + item
        remote_item_path = remote_path + item
        # print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item.filename, stat.S_ISDIR(item.st_mode)))
        if not os.path.isdir(absolute_item_path):
          ## we assume the folder already exists
          print(f'Put file {absolute_item_path} on remote {remote_item_path}')
          if not dry_run: sftpc.put(absolute_item_path, remote_item_path, callback=None, confirm=None)
          continue
        ## create folder on remote with mkdir
        print(f'Creating dir {absolute_item_path} on remote {remote_item_path}')
        if not dry_run: 
          sftpc.mkdir(remote_item_path)
        copy_dir_recursive(absolute_item_path, remote_item_path, dry_run=dry_run)

    local_path_split = [s for s in absolute_local_path.split(os.sep) if s]
    node_name = local_path_split[-1]
    remote_root_path = _path_utils.normalize_path(remote_mount_point) + node_name
    print(f'Creating mount point in remote : {remote_root_path}')
    if not dry_run: 
      try:
        sftpc.mkdir(remote_root_path)
      except IOError as e:
        print(f'Failure creating {remote_root_path}: detail {e}')

    return copy_dir_recursive(absolute_local_path, remote_root_path, dry_run=dry_run)

  def generate_rfile_hash(self, file_path, hexdigest=True, return_also_buffer=False):
    # a = self.agent
    client = self.get_sftp_client()

    print(f'Trying to get {file_path}')

    with client.open(file_path, "rb") as f:
      buf = f.read()

    if hexdigest: 
      dig = hashlib.md5(buf).hexdigest()
    else:
      dig = hashlib.md5(buf).digest()

    if not return_also_buffer: 
      return dig 

    return (io.BytesIO(buf), dig)

  def generate_tree_structure_oversftp(self, root_path:str):

    ROOT_PATH = root_path[:-1] if root_path.endswith(os.sep) else root_path

    def _generate_tree_structure_oversftp(root_path :str):
      '''
        @param             root_path        string
      '''
      if not root_path.endswith(os.path.sep):
        root_path = root_path + os.path.sep

      # a = self.agent
      # treemap = []
      sftpc = self.get_sftp_client()

      treemap = {}

      for item in sftpc.listdir_attr(root_path):
        # absolute_item_path = root_path + item.filename
        # print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item.filename, stat.S_ISDIR(item.st_mode)))
        # if stat.S_ISDIR(item.st_mode):
        #   rtreemap[item.filename] = self.generate_tree_hash_oversftp(absolute_item_path)
        # else:
        #   rtreemap[item.filename] = self.generate_rfile_hash(absolute_item_path)
        ##  exclude folder for the tree
        if item in ['.masy']:
          continue
        absolute_item_path = root_path + item.filename
        # print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item, os.path.isdir(absolute_item_path)))
        k = absolute_item_path.replace(ROOT_PATH, '.')
        treek = _skey.gen_key_tree(k, item.st_mode)
        if not stat.S_ISDIR(item.st_mode):
          treemap[treek] = ""
          continue

        treemap[treek] = _generate_tree_structure_oversftp(absolute_item_path)

      return treemap

    return _generate_tree_structure_oversftp(root_path)

  def generate_tree_structure_hash_oversftp(self, root_path: str):
    root_path = _path_utils.normalize_path(root_path)
    tree = self.generate_tree_oversftp(root_path)
    pickled = pickle.dumps(tree)
    return hashlib.sha256(pickled).hexdigest()


  def generate_tree_hash_oversftp(self, root_path :str):
    '''
      @param             root_path        string, root_path in remote server
      generate a map of hashes starting from `root_path` recursively
    '''


    rtreemap = {}

    if not root_path.endswith(os.path.sep):
      root_path = root_path + os.path.sep

    # a = self.agent
    sftpc = self.get_sftp_client()

    for item in sftpc.listdir_attr(root_path):
      absolute_item_path = root_path + item.filename
      print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item.filename, stat.S_ISDIR(item.st_mode)))
      treek = _skey.gen_key_tree(item.filename, item.st_mode)
      if stat.S_ISDIR(item.st_mode):
        rtreemap[treek] = self.generate_tree_hash_oversftp(absolute_item_path)
      else:
        rtreemap[treek] = self.generate_rfile_hash(absolute_item_path)

    return rtreemap

  def generate_file_hash_oversftp(self, file_path: str, return_also_buffer: bool=False):
    sftpc = self.get_sftp_client()
    return self.generate_rfile_hash(file_path, return_also_buffer=return_also_buffer)

  async def generate_rfile_hash_async(self, file_path, hexdigest=True):
    # a = self.agent
    client = self.get_sftp_client()

    with client.open(file_path, "rb") as f:
      buf = f.read()

    if hexdigest: return hashlib.md5(buf).hexdigest()
    return hashlib.md5(buf).digest()

  async def generate_tree_hash_oversftp_async(self, root_path :str):
    '''
      @param             root_path        string, root_path in remote server
      generate a map of hashes starting from `root_path` recursively
    '''
    # if not os.path.isdir(root_path):
    #   raise Exception('Provide a valid folder to start the hashing')
    #
    # if not root_path.endswith(os.path.sep):
    #   root_path = root_path + os.path.sep
    # root_path = check_isdir(root_path)

    rtreemap = {}

    if not root_path.endswith(os.path.sep):
      root_path = root_path + os.path.sep

    # a = self.agent
    sftpc = self.get_sftp_client()

    def dtask_done_cback(fname, st_mode, f):
      print('dtask done %s' % (fname,))
      treek = _skey.gen_key_tree(fname, st_mode)
      rtreemap[treek] = f.result()

    def ftask_done_cback(fname, st_mode, f):
      print('ftask done %s' % (fname,))
      treek = _skey.gen_key_tree(fname, st_mode)
      rtreemap[treek] = f.result()

    # futures_map = {}
    tasks = []
    for item in sftpc.listdir_attr(root_path):
      absolute_item_path = root_path + item.filename
      print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item.filename, stat.S_ISDIR(item.st_mode)))
      if stat.S_ISDIR(item.st_mode):
        # rtreemap[item.filename] = await generate_tree_hash_oversftp_async(absolute_item_path)
        dtask = asyncio.create_task(self.generate_tree_hash_oversftp_async(absolute_item_path))
        dtask.add_done_callback(functools.partial(dtask_done_cback, item.filename, item.st_mode))
        tasks.append(dtask)
      else:
        # rtreemap[item.filename] = await generate_rfile_hash_async(absolute_item_path, client=sftpc)
        ftask = asyncio.create_task(self.generate_rfile_hash_async(absolute_item_path))
        ftask.add_done_callback(functools.partial(ftask_done_cback, item.filename, item.st_mode))
        tasks.append(ftask)
        # item.filename

    await asyncio.gather(*tasks)
    return rtreemap

  def check_rfile_status(self, file_path:str):
    try:
      print(f'checkin rfile status {file_path}')
      iobuf, dig = self.generate_rfile_hash(file_path, return_also_buffer=True)
      return {
          'exists' : True,
          'iobuffer' : iobuf,
          'hash': dig,
          }
    except IOError as e:
      if e.errno == errno.ENOENT: return {
          'exists' : False,
          'iobuffer' : None,
          'hash': None
          }
    raise Exception(f'Something went wrong and strange {file_path}')

  def check_rfolder_status(self, path:str):
    try:
      print(f'checkin rfolder status {path}')
      dig = self.generate_tree_hash_oversftp(path)
      return {
          'exists' : True,
          'iobuffer' : None,
          'hash': dig,
          }
    except IOError as e:
      if e.errno == errno.ENOENT: return {
          'exists' : False,
          'iobuffer' : None,
          'hash': None
          }
    raise Exception(f'Something went wrong and strange {path}')

def test_sync_rtree(path='/home/notanamber/notes_dev/'):
  start = time.time()
  print('Start task')
  agent = AdvancedSftpClientAgent('myvps', 'notanamber')
  rtree = agent.generate_tree_hash_oversftp(path)
  end = time.time()
  print('task done in %.2f' % (end - start))
  return rtree

def test_async_rtree(path='/home/notanamber/notes_dev/'):
  start = time.time()
  print('Start task')
  agent = AdvancedSftpClientAgent('myvps', 'notanamber')
  rtree = asyncio.run(agent.generate_tree_hash_oversftp_async(path)) 
  end = time.time()
  print('task done in %.2f' % (end - start))
  return rtree


  '''
  a
   - b
     - c
       - k.txt
     i.txt
     g.txt
  j.txt
  k.txt

  tree['a'] = {
      'b' : {

        },
      'j.txt' : '012349jasdfh9934',


      }

  '''

# synca = SyncAgent()
# sftpc = a.get_sftp_client()
