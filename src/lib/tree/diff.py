from lib.snapshot import key as _skey


def recursive_raw_structure_diff(tree_a, tree_b, tree_a_tag, tree_b_tag, res):
  for key_a, hsh_a in tree_a.items():
    hsh_b = tree_b.get(key_a)

    item_name_a, st_mode_a = _skey.split_key_tree(key_a)

    if hsh_b is None:
      res[f'{item_name_a}'] = {
        tree_a_tag : 'x',
      }
      res[f'{item_name_a}']['%s_type' % (tree_a_tag,)] = st_mode_a
      continue

    if isinstance(hsh_a, str):
      if hsh_a == hsh_b: continue

      res[f'{item_name_a}'] = {
        tree_a_tag : 'x',
        tree_b_tag : 'x',
      }

      res[f'{item_name_a}']['%s_hash' %(tree_a_tag,)] = hsh_a
      res[f'{item_name_a}']['%s_hash' %(tree_b_tag,)] = hsh_b

      res[f'{item_name_a}']['%s_type' %(tree_a_tag,)] = st_mode_a
      res[f'{item_name_a}']['%s_type' %(tree_b_tag,)] = st_mode_a
      continue


    recursive_raw_structure_diff(hsh_a, hsh_b,tree_a_tag, tree_b_tag, res)

def raw_structure_diff(tree_a, tree_b, tree_a_tag='tree_a', tree_b_tag='tree_b'):
  '''
    useful to compare the structure of two trees
    it returns a dict where the key is the relative path representing a file or folder givin these informations

      - file present in tree_a     
      - file present in tree_b
      - folder subtree changed
  '''
  res = {}
  recursive_raw_structure_diff(tree_a, tree_b, tree_a_tag=tree_a_tag, tree_b_tag=tree_b_tag, res=res)
  ## note the tree inversion
  recursive_raw_structure_diff(tree_b, tree_a, tree_a_tag=tree_b_tag, tree_b_tag=tree_a_tag, res=res)
  return res
