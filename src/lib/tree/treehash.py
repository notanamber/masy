import stat
import os
import pickle
import hashlib

from lib.snapshot import key as _skey
from lib.utils import path_utils as _path_utils

def generate_tree_structure(root_path :str):

  ROOT_PATH = root_path[:-1] if root_path.endswith(os.sep) else root_path

  def _generate_tree_structure(root_path :str):
    '''
      @param             root_path        string
      generate an ordered list of entries, similar to `find ~/sharednotes_dev/ -print`
      command
    '''
    root_path = _path_utils.normalize_path(root_path)
    items = os.listdir(root_path)
    
    treemap = {}

    for item in sorted(items):
      ##  exclude folder for the tree
      if item in ['.masy']:
        continue
      absolute_item_path = root_path + item
      print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item, os.path.isdir(absolute_item_path)))

      k = absolute_item_path.replace(ROOT_PATH, '.')
      stat_item = os.stat(absolute_item_path)
      treek = _skey.gen_key_tree(k, stat_item.st_mode)
      if not stat.S_ISDIR(stat_item.st_mode):
        treemap[treek] = ''
        continue

      treemap[treek] = _generate_tree_structure(absolute_item_path)

    return treemap

  return _generate_tree_structure(root_path)

def generate_tree_structure_hash(root_path: str):
  root_path = _path_utils.check_isdir(root_path)
  root_path = _path_utils.normalize_path(root_path)
  base_path = root_path
  tree = generate_tree_structure(root_path)
  tree = [t.replace(base_path, '.%s' % (os.sep)) for t in tree]
  pickled = pickle.dumps(tree)
  return hashlib.sha256(pickled).hexdigest()
