import gzip
import json
from lib.utils import path_utils as _path_utils
from lib.snapshot.generate import local as _genlocal

DUMP_FILE_NAME = '.snapshot.json.gz'
SNAPSHOT_PATH = '../'

def dump_snapshot(snapshot, path=None, dump_file_name=None):
  path = path or SNAPSHOT_PATH
  path = _path_utils.check_isdir(path)

  dump = json.dumps(snapshot)
  dump = gzip.compress(dump.encode())

  dump_file_name = dump_file_name or DUMP_FILE_NAME

  file = '%s%s' % (path, dump_file_name)

  with open(file, "wb") as f:
    f.write(dump)

def load_snapshot(path, dump_file_name=None):
  dump_file_name = dump_file_name or DUMP_FILE_NAME
  file = '%s%s' % (path, dump_file_name)

  with open(file, "rb") as f:
    dump = f.read()

  snapshot = gzip.decompress(dump)
  snapshot = json.loads(snapshot)
  return snapshot

  
def decode_snapshot(path=None, dump_file_name=None):
  path = path or SNAPSHOT_PATH
  path = _path_utils.check_isdir(path)

  dump_file_name = dump_file_name or DUMP_FILE_NAME

  file = '%s%s' % (path, dump_file_name)
  with open(file, "rb") as f:
    buf = f.read()

  buf = gzip.decompress(buf)
  return json.loads(buf)
