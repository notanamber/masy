import os
from lib.snapshot import key as _skey

def recursive_raw_diff(tree_a, tree_b, tree_a_tag, tree_b_tag, path, res):
  for key_a, hsh_a in tree_a.items():
    hsh_b = tree_b.get(key_a)

    item_name_a, st_mode_a = _skey.split_key_tree(key_a)

    if hsh_b is None:
      res[f'{path}{item_name_a}'] = {
        tree_a_tag : 'x',
      }
      res[f'{path}{item_name_a}']['%s_type' % (tree_a_tag,)] = st_mode_a
      res[f'{path}{item_name_a}']['%s_hash' % (tree_a_tag,)] = hsh_a
      continue


    if isinstance(hsh_a, str):
      if hsh_a == hsh_b: continue

      res[f'{path}{item_name_a}'] = {
        tree_a_tag : 'x',
        tree_b_tag : 'x',
      }

      res[f'{path}{item_name_a}']['%s_hash' %(tree_a_tag,)] = hsh_a
      res[f'{path}{item_name_a}']['%s_hash' %(tree_b_tag,)] = hsh_b
      res[f'{path}{item_name_a}']['%s_type' %(tree_a_tag,)] = st_mode_a
      res[f'{path}{item_name_a}']['%s_type' %(tree_b_tag,)] = st_mode_a
      continue


    recursive_raw_diff(hsh_a, hsh_b,tree_a_tag, tree_b_tag, '%s%s/' % (path, item_name_a,), res)

def raw_diff(tree_a, tree_b, tree_a_tag='tree_a', tree_b_tag='tree_b', path='./'):
  '''
    it returns a dict where the key is the relative path of node givin these informations

      - node present in tree_a     
      - node present in tree_b
      - hash is the same or differ
  '''
  res = {}
  if not path.endswith(os.sep): path = path + os.sep
  recursive_raw_diff(tree_a, tree_b, tree_a_tag=tree_a_tag, tree_b_tag=tree_b_tag, path=path, res=res)
  ## note the tree inversion
  recursive_raw_diff(tree_b, tree_a, tree_a_tag=tree_b_tag, tree_b_tag=tree_a_tag, path=path, res=res)
  return res
