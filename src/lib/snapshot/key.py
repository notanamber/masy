SEP = '_'


def gen_key_tree(item_name: str, st_mode: int):
  """Generate a key to use in snapshot tree"""
  return f'{item_name}{SEP}{st_mode}'


def split_key_tree(k: str):
  """Split the key used in snapshot tree in (item_name, st_mode)"""
  item_name, st_mode = k.rsplit(SEP, 1)
  return (item_name, int(st_mode))
