import stat 
import os
import errno
import hashlib 
import time
import json
import io
import gzip

import asyncio
import functools

# from lib.sclient.agent import SftpClientAgent


'''
  DO NOT USE THIS: move this methods in AdvancedSftpClientAgent
'''

class RHAgent():
  '''
    Give you an sftp connection that you can use for generate remote file hash
    and as a proxy for use SftpClientAgent funcionality
  '''
  def __init__(self, hostname, username):
    ## load ssh config and read the ip of hostname
    self.hostname = hostname
    self.username = username
    self.agent = SftpClientAgent(hostname, username)

  def remove_dir(self, absolute_path):
    '''
      Here we assume that absolute_path on remote can be not empty
      XXX: the ftp client can remove only empty folder
    '''
    pass

  def get_sftp_client(self):
    a = self.agent
    return a.get_sftp_client()

  def generate_rfile_hash(self, file_path, hexdigest=True, return_also_buffer=False):
    # a = self.agent
    client = self.get_sftp_client()

    print(f'Trying to get {file_path}')

    with client.open(file_path, "rb") as f:
      buf = f.read()

    if hexdigest: 
      dig = hashlib.md5(buf).hexdigest()
    else:
      dig = hashlib.md5(buf).digest()

    if not return_also_buffer: 
      return dig 

    return (io.BytesIO(buf), dig)

  def check_rfile_status(self, file_path:str):
    try:
      iobuf, dig = self.generate_rfile_hash(file_path, return_also_buffer=True)
      return {
          'exists' : True,
          'iobuffer' : iobuf,
          'hash': dig,
          }
    except IOError as e:
      if e.errno == errno.ENOENT: return {
          'exists' : False,
          'iobuffer' : None,
          'hash': None
          }
    raise Exception(f'Something went wrong and strange {file_path}')

  def check_rfolder_status(self, path:str):
    try:
      dig = self.generate_tree_hash_oversftp(path)
      return {
          'exists' : True,
          'iobuffer' : None,
          'hash': dig,
          }
    except IOError as e:
      if e.errno == errno.ENOENT: return {
          'exists' : False,
          'iobuffer' : None,
          'hash': None
          }
    raise Exception(f'Something went wrong and strange {path}')

  ## move in SftpClientAgent
  def copy_file_to_remote(self, localpath, remotepath, callback=None, confirm=False):
    '''
      put(localpath, remotepath, callback=None, confirm=True)
    '''
    def def_cback():
      print(f'{localpath} copied to {remotepath}, success')

    callback = callback if callback else def_cback
    client = self.get_sftp_client()
    client.put(localpath, remotepath, callback=callback, confirm=confirm)
    return 


  def generate_tree_hash_oversftp(self, root_path :str):
    '''
      @param             root_path        string, root_path in remote server
      generate a map of hashes starting from `root_path` recursively
    '''


    rtreemap = {}

    if not root_path.endswith(os.path.sep):
      root_path = root_path + os.path.sep

    # a = self.agent
    sftpc = self.get_sftp_client()

    for item in sftpc.listdir_attr(root_path):
      absolute_item_path = root_path + item.filename
      print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item.filename, stat.S_ISDIR(item.st_mode)))
      if stat.S_ISDIR(item.st_mode):
        rtreemap[item.filename] = {
          'hash': self.generate_tree_hash_oversftp(absolute_item_path),
          'st_mode': item.st_mode,
        }
      else:
        rtreemap[item.filename] = {
          'hash': self.generate_rfile_hash(absolute_item_path),
          'st_mode': item.st_mode,
        }

    return rtreemap

  def generate_file_hash_oversftp(self, file_path: str, return_also_buffer: bool=False):
    # a = self.agent
    sftpc = self.get_sftp_client()
    return self.generate_rfile_hash(file_path, return_also_buffer=return_also_buffer)

  async def generate_rfile_hash_async(self, file_path, hexdigest=True):
    # a = self.agent
    client = self.get_sftp_client()

    with client.open(file_path, "rb") as f:
      buf = f.read()

    if hexdigest: return hashlib.md5(buf).hexdigest()
    return hashlib.md5(buf).digest()

  async def generate_tree_hash_oversftp_async(self, root_path :str):
    #XXX: Adapt with the new key -> (item, stmode)
    '''
      @param             root_path        string, root_path in remote server
      generate a map of hashes starting from `root_path` recursively
    '''
    # if not os.path.isdir(root_path):
    #   raise Exception('Provide a valid folder to start the hashing')
    #
    # if not root_path.endswith(os.path.sep):
    #   root_path = root_path + os.path.sep
    # root_path = check_isdir(root_path)

    rtreemap = {}

    if not root_path.endswith(os.path.sep):
      root_path = root_path + os.path.sep

    # a = self.agent
    sftpc = self.get_sftp_client()

    def dtask_done_cback(fname, st_mode, f):
      print('dtask done %s' % (fname,))
      rtreemap[fname] = {
        'hash': f.result(),
        'st_mode': st_mode,
      }

    def ftask_done_cback(fname, st_mode, f):
      print('ftask done %s' % (fname,))
      rtreemap[fname] = {
        'hash': f.result(),
        'st_mode': st_mode,
      }

    # futures_map = {}
    tasks = []
    for item in sftpc.listdir_attr(root_path):
      absolute_item_path = root_path + item.filename
      print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item.filename, stat.S_ISDIR(item.st_mode)))
      if stat.S_ISDIR(item.st_mode):
        # rtreemap[item.filename] = await generate_tree_hash_oversftp_async(absolute_item_path)
        dtask = asyncio.create_task(self.generate_tree_hash_oversftp_async(absolute_item_path))
        dtask.add_done_callback(functools.partial(dtask_done_cback, item.filename, item.st_mode))
        tasks.append(dtask)
      else:
        # rtreemap[item.filename] = await generate_rfile_hash_async(absolute_item_path, client=sftpc)
        ftask = asyncio.create_task(self.generate_rfile_hash_async(absolute_item_path))
        ftask.add_done_callback(functools.partial(ftask_done_cback, item.filename, item.st_mode))
        tasks.append(ftask)
        # item.filename
    
    await asyncio.gather(*tasks)
    return rtreemap

def test_sync_rtree(path='/home/notanamber/notes_dev/'):
  start = time.time()
  print('Start task')
  rhagent = RHAgent('myvps', 'notanamber')
  rtree = rhagent.generate_tree_hash_oversftp(path)
  end = time.time()
  print('task done in %.2f' % (end - start))
  return rtree

def test_async_rtree(path='/home/notanamber/notes_dev/'):
  start = time.time()
  print('Start task')
  rhagent = RHAgent('myvps', 'notanamber')
  rtree = asyncio.run(rhagent.generate_tree_hash_oversftp_async(path)) 
  end = time.time()
  print('task done in %.2f' % (end - start))
  return rtree

def get_test_agent():
  rhagent = RHAgent('myvps', 'notanamber')
  a = rhagent
  return a


  '''
  a
   - b
     - c
       - k.txt
     i.txt
     g.txt
  j.txt
  k.txt

  tree['a'] = {
      'b' : {

        },
      'j.txt' : '012349jasdfh9934',


      }

  '''




