import os
import io
import hashlib 
import json
import gzip

from lib.utils import path_utils as _path_utils
from lib.snapshot import key as _skey


def generate_file_hash(file_path, hexdigest=True, return_also_buffer=False):
  with open(file_path, "rb") as f:
    buf = f.read()

  if hexdigest: 
    if not return_also_buffer: return hashlib.md5(buf).hexdigest()
    return (hashlib.md5(buf).hexdigest(), io.BytesIO(buf))

  if not return_also_buffer: return hashlib.md5(buf).digest()
  return (hashlib.md5(buf).digest(), io.BytesIO(buf))


def generate_tree_hash(root_path :str):
  '''
    @param             root_path        string
    generate a map of hashes starting from `root_path` recursively
    #XXX: note the key is a string with (item_stat_type) -> read os.lstat
  '''
  root_path = _path_utils.check_isdir(root_path)

  treemap = {}

  items = os.listdir(root_path)
  for item in items:
    ##  exclude folder for the tree
    if item in ['.masy']:
      continue
    absolute_item_path = root_path + item
    absolute_item_path_stat = os.lstat(absolute_item_path)
    absolute_item_path_stat_type = absolute_item_path_stat.st_mode
    print('absolute_item_path: %s, item %s, isdir: %s' % (absolute_item_path, item, os.path.isdir(absolute_item_path)))
    treek = _skey.gen_key_tree(item, absolute_item_path_stat_type)
    if os.path.isdir(absolute_item_path):
      treemap[treek] = generate_tree_hash(absolute_item_path)
    else:
      treemap[treek] = generate_file_hash(absolute_item_path)

  return treemap


'''
a
 - b
   - c
     - k.txt
   i.txt
   g.txt
j.txt
k.txt

tree['a'] = {
    'b' : {

      },
    'j.txt' : '012349jasdfh9934',


    }

'''

root_path = '/home/luca/rsyn_test_fap'



