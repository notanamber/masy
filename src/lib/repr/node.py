import os
import stat

from dataclasses import dataclass
from lib.utils import path_utils as _path_utils

from lib.sclient.agent import AdvancedSftpClientAgent


@dataclass
class TreeNode:
  """
  Basic definition of a node in a tree,
  TreeNode can model both a local and remote node

    @param          base_path          base_path of node: NOTE can be local or remote

    @param          name               name of node
    @param          rel_path           relative path of node respect of root of shared folder
    @param          cur_type           is the current type in local tree
    @param          cur_hash           is the current hash in local tree

  """
  name: str
  rel_path: str
  base_path: str = None
  cur_type: str = None
  cur_hash: str = None

  def absolute_path(self):
    assert self.base_path
    rel_path = self.rel_path
    name = self.name
    abs_path = '%s%s' % (rel_path, name)
    abs_path = abs_path.replace('.%s' % (os.sep), self.base_path)
    return abs_path

  def relative_path(self):
    rel_path = self.rel_path
    name = self.name
    rel_path = '%s%s' % (rel_path, name)
    return rel_path

  def is_dir(self):
    # rattr = self.get_rattr()
    if stat.S_ISDIR(self.cur_type):
      return True
    return False

  def is_file(self):
    # rattr = self.get_rattr()
    if stat.S_ISREG(self.cur_type):
      return True
    return False


class RemoteTreeNode(TreeNode):
  def __init__(self,
               name,
               rel_path,
               base_path=None,
               cur_type=None,
               cur_hash=None,
               agent: AdvancedSftpClientAgent = None,
               hostname: str = None,
               username: str = None):

    TreeNode.__init__(self, name, rel_path,
                      base_path=base_path,
                      cur_type=cur_type,
                      cur_hash=cur_hash)
    # agent -> AdvancedSftpClientAgent
    # agent is used to perform remote instrospection
    self.agent = agent
    self._rattr = None
    if agent is None:
      assert hostname and username
      self.agent = AdvancedSftpClientAgent(hostname, username)

  def get_rattr(self):
    """use agent to get attributes of remote node"""
    if self._rattr is not None:
      return self._rattr

    a = self.agent
    abs_path = self.absolute_path()
    rattr = a.rpath_attr(abs_path)
    self._rattr = rattr
    return self._rattr

  def is_dir(self):
    rattr = self.get_rattr()
    if stat.S_ISDIR(rattr.st_mode):
      return True
    return False

  def is_file(self):
    rattr = self.get_rattr()
    if stat.S_ISREG(rattr.st_mode):
      return True
    return False


@dataclass
class NodeToSync():

  """
    A Node is a representation of a `Local` node in a local tree
    that we wish to keep synchronised with remote source

    @param          last_type          is the last type received from server
    @param          last_hash          is the last hash received from server
                                       (a directory can have or empty hash or a subtree)
    @param          remote_hash        is the remote hash (usually computed with a remote call)
                                       no conflict -> `remote_hash` == `last_hash`
                                       conflict    -> `remote_hash` != `last_hash`
                                                      this means we have lost a version of node

    compute the action actually binded to node
    can be -> deletion | addition | change
  """

  def __init__(self,
               LNode: TreeNode,
               RNode: RemoteTreeNode,
               last_type: str = None,
               last_hash: str = None):
    self._lnode = LNode
    self._rnode = RNode
    self.last_type = last_type
    self.last_hash = last_hash

  def is_node_added(self):
    return self.last_hash is None and self.cur_hash is not None

  def is_node_deleted(self):
    return self.cur_hash is None and self.last_hash is not None

  def is_node_changed(self):
    return self.cur_hash is not None and self.last_hash is not None

  def get_action(self):
    if self.is_node_added():
      return 'addition'
    elif self.is_node_deleted():
      return 'deletion'
    elif self.is_node_changed():
      return 'changed'


  def is_file(self):
    if self.is_node_deleted():
      return self.last_type == 'f'
    ## if is_node_added or is_node_changed
    return self.cur_type == 'f'

  def is_dir(self):
    if self.is_node_deleted():
      return self.last_type == 'd'
    ## if is_node_added or is_node_changed
    return self.cur_type == 'd'

  def get_type(self):
    if self.is_file: return 'f'
    return 'd'


def node_factory(d, agent, local_base_path=None, remote_base_path=None):
  if local_base_path:
    loca_base_path = _path_utils.normalize_path(local_base_path)

  if remote_base_path:
    remote_base_path = _path_utils.normalize_path(remote_base_path)

  name = d['name']
  rel_path = _path_utils.normalize_path(d['rel_path'])

  last_type = d.get('last_type')
  cur_type = d.get('cur_type')
  last_hash = d.get('last_hash')
  cur_hash = d.get('cur_hash')
  # XXX: we don't have a remote hash for the remote node
  # we expect to calculate it on the fly using `RemoteTreeNode` class
  lnode = TreeNode(name, rel_path, local_base_path, cur_type, cur_hash)
  rnode = RemoteTreeNode(name, rel_path, remote_base_path, agent=agent)
  return NodeToSync(lnode, rnode, last_type=last_type, last_hash=last_hash)

