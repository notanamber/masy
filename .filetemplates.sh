#!/bin/bash
# Copyright 2021 Luca Paris
#This file is part of masync.

#masync is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#masync is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with masync.  If not, see <http://www.gnu.org/licenses/>.

###############################################
# the file contains all sync managed by masync
###############################################
SYNCFILE=~/.syncdir.sync
###############################################
# the file contains the snapshot. A snapshot is the list of files 
# locally synchronized when the sync was stopped
###############################################
SNAPSHOTFILE=~/.snapshot_{{hash}}
TMPQUEUEFILE=~/.syncdir_{{hash}}.queue
TMPQUEUEDELETES=~/.syncdir_{{hash}}.deletes
###############################################
# the file when the loop logs the activity
###############################################
SYNCLOOPFILE=~/syncloop_{{hash}}.nohup
###############################################
# raw rsync log (used by .synccmd.sh)
###############################################
LOGFILERSYNC=~/syncdir_{{hash}}.log

################################
#  $1 -> instr, the string template to format, we expect it contains placeholders {{placeholder_name}}
#  $[2..n] -> placeholder in the form placeholder_name=value
################################

format() {
  instr=$1
  shift
  for arg in "$@"; do
    IFS== read -r key val <<< $arg
    instr=$(echo $instr | sed -e "s/{{$key}}/$val/g")
  done
  echo $instr
}
